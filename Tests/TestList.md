# Test Lists

## CountDownTime Tests
- [x] Initialization
- [x] 90 second side
- [x] One other side
- [x] below 0 degrees
- [x] above 360 degrees
- [x] 0 degrees
- [x] 360 degrees

## Test Main Functions

- [x] timesUp() returns true
  - [x] Mock Serial.printLn
  - [x] Mock playTone
- [x] getGyroDataInterrupt() returns true
- [x] writeToDisplay() returns true

## TDD Tests for Gyro Drift



