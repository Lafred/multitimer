#include <CppUTest/CommandLineTestRunner.h>

#include "./TEST_CountDownTime.cpp"
#include "./TEST_MainFunctions.cpp"

int main(int ac, char** av)
{
   return CommandLineTestRunner::RunAllTests(ac, av);
}