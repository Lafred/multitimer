#include <CppUTest/TestHarness.h>

#include "../MainFunctions.h"

TEST_GROUP(MainFunctions)
{
};

TEST(MainFunctions, timesUp)
{
    TIMES_UP = true;
    for (int i=0; i < 16; i++)
    {
        CHECK_EQUAL(true, timesUp(nullptr));
    }
}

TEST(MainFunctions, getGyroDataInterrupt)
{
    CHECK_EQUAL(true, getGyroDataInterrupt(nullptr));
}

TEST(MainFunctions, writeToDisplay)
{
    CHECK_EQUAL(true, writeToDisplay(nullptr));
}