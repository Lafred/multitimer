#include <CppUTest/TestHarness.h>

#include "../CountDownTime.h"

TEST_GROUP(CountDownTimeClass) 
{
    CountDownTime timer;
};

TEST(CountDownTimeClass, Init)
{
    LONGS_EQUAL(timer.getMin(), 0);
    LONGS_EQUAL(timer.getSec(), 10);
}

TEST(CountDownTimeClass, checkStartSide)
{
    timer.checkPosition(0);
    LONGS_EQUAL(timer.getMin(), 0);
    LONGS_EQUAL(timer.getSec(), 90);
}

TEST(CountDownTimeClass, check60MinSide)
{
    timer.checkPosition( 360.0/7.0 * 6.0 + 5.0);
    LONGS_EQUAL(timer.getMin(), 60);
    LONGS_EQUAL(timer.getSec(), 0);
}

TEST(CountDownTimeClass, checkBottomBorder)
{
    timer.checkPosition( 0);
    LONGS_EQUAL(timer.getMin(), 0);
    LONGS_EQUAL(timer.getSec(), 90);
}

TEST(CountDownTimeClass, checkTopBorder)
{
    timer.checkPosition( 360 );
    LONGS_EQUAL(timer.getMin(), 60);
    LONGS_EQUAL(timer.getSec(), 0);
}

TEST(CountDownTimeClass, checkUnder0)
{
    timer.checkPosition( -15 );
    LONGS_EQUAL(timer.getMin(), 60);
    LONGS_EQUAL(timer.getSec(), 0);
}

TEST(CountDownTimeClass, checkOver360)
{
    timer.checkPosition( 375.0);
    LONGS_EQUAL(timer.getMin(), 0);
    LONGS_EQUAL(timer.getSec(), 90);
}
