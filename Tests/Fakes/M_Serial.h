#include <iostream>
#include <string>

class serial
{
  public:

    bool println(std::string input)
    {
        std::cout << input << std::endl;
        return true;
    }

    bool println(double input)
    {
        std::cout << input << std::endl;
        return true;
    }
};
