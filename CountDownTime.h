#ifndef COUNT_DOWN_TIME
#define COUNT_DOWN_TIME
// #include <Arduino.h>

class CountDownTime
{
public:
    enum Side { ninetySec, fiveMin, tenMin, fifteenMin, thirtyMin, fortyFiveMin, sixtyMin };
private:
    static Side CurrentSide;
    static int minutes;
    static int seconds;
public:
    CountDownTime();
    ~CountDownTime();
    static int checkPosition(double currentAngle);
    static int getMin();
    static int getSec();
};

#endif