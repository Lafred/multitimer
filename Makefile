
all: 
	g++ -g3 -c CountDownTime.cpp

test: all
	cd Tests && make && ./a.out

clean:
	rm Tests/Fakes/*.gch *.o Tests/a.out
