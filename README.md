# Vision

I need a portable timer so that I can better manage my time and focus.  I have difficulty keeping track of time and often get worn out and arrive late.

I'd like a timer that rotates and as it does so, it starts a timer for a specific amount of time.  I've seen several such timers and would like to make one optimized for me.  I'd like it to keep tracking how long it has been since it went off, so I know how long I've gone over on a particular task.  I'd like it to have a somewhat loud buzzer that I can put to a lower setting.  

I'd like this timer to have the following times:  
~~1. 1 min~~  
2. 90 sec  
3. 5 min  
4. 10 min  
5. 15 min  
~~6. 20 min~~  
7. 30 min  
8. 45 min  
9. 60 min  

If I cut out 1 minute, I can have 8 sides and an off setting.  I could try 90 seconds instead of 1 or 3 minutes.  I'd like to have 7 sides so I am taking out 20 minutes.  I could do a five minute chirp for timer overruns.

I'd like it to be fairly small so it is portable.  It needs to be a dedicated device so I don't get distracted.  It can't be too small either.  Probably about the size of a hand.

## Arduino Timer Tasks / Planning

### Hardware and Code Integration
- [x] Buzzer
- [x] LCD Screen
- [x] Gyro Sensor
- [x] RTC

### Code
- [x] arduino-timer integration
- [x] Improve code quality for reuse and stability
  - [x] Unit test CountDownTime Class
  - [x] Separate and Unit test what I can from Timer.ino
- [ ] Correct rotation direction
- [ ] Calibrate IMU
- [ ] Create an algorythm to detect larger movements and ignore drift.  
- [ ] Remove or refactor Serial.print statements.
  - [ ] remove Serial.print statements from interrupts 
  **OR**
  - [ ] Find a way to Serial.print only when needed.

### Mechanical
- [ ] 3D Printed Case Model
  - [x] Case Front
  - [ ] Case Back
- [ ] 3D Print
  - [x] Case Front
  - [ ] Case back

## Bonus

- [ ] Temperature reader
- [ ] pressure sensor
- [ ] humidity sensor
- [ ] WiFI
- [ ] Web App

## 6/15/2021 
Successfully integrated with arduino-timer library.  Successfully compiled with some unit tests using the CppUTest framework.  Created several fakes for library and hardware dependencies.  Run make test to run completed unit tests on x86_64 architecture.  I also fixed VS Code intellisense for Fedora 34.  

Learning to unit test Arduino code has taken a lot of work over the last few months.  Found some useful links on embedded unit testing after talking to Chad M.  
* https://interrupt.memfault.com/tag/better-firmware/

I first unit tested a class with no dependencies, CounntDownTime.h.  This was relatively simple.  I then separated what I could from Timer.ino and began creating fakes and reorganizing code until I got MainFunctions.h to compile with a x86_64 Operating System.  

I really learned about the precomiler's -I flags and the linker's -l flags and how to use them to change which libraries are included.  For testing I include my fakes, they may be stubs, and for Arduino compilation the Arduino libraries are included.  I still need to write the actual tests for MainFunctions.h.

I currently have a semi-working timer.  It drifts to a different time every about 10 minutes and the numbers are currently backwards.  These two problems should be fixed fairly easily with software.  The wrong rotation I can fix by a properly placed negative sign I believe.  The rotation problem can be fixed by properly calibrating the timer and using a bit of AI.  I'm thinking of testing for slow drift and if the drift is slow enough putting the *position* variable on the center of the current side.  

I need to create a back for my timer soon.

I switched hardware to an Arduino nano for simplicity and cost.  Arduino 33 IOT's cost about twenty bucks where you can get several Nano's for ten bucks or so.  The arduino Nano runs at 5 v and needs a *logic level converter* for the 6 axis gyroscope.  

## 1/29/2021
Discovered that I need to better vet the Arduino libraries I use.  The MillisTimer library is not working as expected.  I found a unit tested arduino-timer library with which I hope to have more success with for interrupts.  It uses a software interrupt and seems to be well supported at the moment.  Submitted a pull request on the changes I made to CuteRobotVoices.  Took a couple walks with my dog.  

## 1/26/2021
Discovered recently that I assumed the interrupt functionality worked originally and discovered the interrupt only runs on a timer.run.  Need to dig more into this and discover how the interrupt library behaves.  It is not well documented for easy use.  

## 1/18/2021
Worked with Nicole and tested last times code.  Got seconds to go down to 59.

Next time need to get serial print statements working, maybe take them out of the interrupt.  

## 1/13/2021

Created rudimentary class for checking device side.  Need work on loop telling seconds to go back to 59.

## 10/26/2020

Reviewed Code, began MillisTimer Integration

