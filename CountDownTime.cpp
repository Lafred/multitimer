#include "CountDownTime.h"

CountDownTime::Side CountDownTime::CurrentSide = ninetySec;
int CountDownTime::minutes = 0;
int CountDownTime::seconds = 5;


CountDownTime::CountDownTime()
{
	CurrentSide = ninetySec;
	minutes = 0;
	seconds = 10;
}

CountDownTime::~CountDownTime()
{ 
}

int CountDownTime::checkPosition(double currentAngle) 
{
	float angleOfRotation = 360/7;  //51.4286 for 7 sides
	
	// currentAngle = currentAngle * 1.6;

	while (currentAngle < 0)
	{
		currentAngle = currentAngle + 360;
	}

	while (currentAngle > 360)
	{
		currentAngle = currentAngle - 360;
	}

	if (currentAngle <= angleOfRotation) {
		CurrentSide = ninetySec;
		minutes = 0;
		seconds = 90;
	}
	else if (angleOfRotation < currentAngle && currentAngle <= angleOfRotation*2) {
		CurrentSide = fiveMin;
		minutes = 5;
		seconds = 0;
	}
	else if(angleOfRotation*2 < currentAngle && currentAngle <= angleOfRotation*3) {
		CurrentSide = tenMin;
		minutes = 10;
		seconds = 0;
	}
	else if(angleOfRotation*3 < currentAngle && currentAngle <= angleOfRotation*4) {
		CurrentSide = fifteenMin;
		minutes = 15;
		seconds = 0;
	}
	else if(angleOfRotation*4 < currentAngle && currentAngle <= angleOfRotation*5) {
		CurrentSide = thirtyMin;
		minutes = 30;
		seconds = 0;
	}
	else if(angleOfRotation*5 < currentAngle && currentAngle <= angleOfRotation*6) {
		CurrentSide = fortyFiveMin;
		minutes = 45;
		seconds = 0;
	}
	else if (currentAngle > angleOfRotation*6) {
		CurrentSide = sixtyMin;
		minutes = 60;
		seconds = 0;
	}

	return CurrentSide;
}

int CountDownTime::getSec(){
	return seconds;
}

int CountDownTime::getMin()
{
	return minutes;
}