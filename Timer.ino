#include <Arduino.h>
// Buzzer Libraries
#include <CuteBuzzerSounds.h>
#include <Sounds.h>
// Seven Segment Display
#include <TM1637.h>
// Gyro and accelerometer
// #include <Arduino_LSM6DS3.h>
#include <SparkFunLSM6DS3.h>
#include <Wire.h>
// Timer Interupts
#include "arduino-timer.h"

//#include<bits/stdc++.h> 
  

#include "CountDownTime.h"

// Used Pins
#define DISPLAY_CLK 2
#define DISPLAY_DIO 3
#define BUZZER_PIN 7

#include "MainFunctions.h"

// WiFi and Internal RTC for WiFi
//#include <WiFiUdp.h>
//#include <RTCZero.h>
//#include "arduinoSecrets.h"

Timer <3> timer1;

void setup() 
{
	// init buzzer
	cute.init(BUZZER_PIN);
	
	// init display
	tm1637.init();
	tm1637.setBrightness(1);

	// Init Serial Port
	Serial.begin(9600);
	
	// init gyro module or iniertial Measurement Unit
	if ( IMU.begin() != 0 ) {
		Serial.println("Failed to initialize IMU!");

		while (1);
	}

	Serial.print("Gyroscope sample rate = ");
	// Serial.print(IMU.());
	Serial.println(" Hz");
	
	Serial.println();
	Serial.println("Gyroscope in degrees/second");
	Serial.println("X\tY\tZ");

	delay(1000);

	// Set Timer Interupt details
	timer1.every(20, getGyroDataInterrupt);
	timer1.every(1000, writeToDisplay);
	timer1.every(500, timesUp);

	//aunit::TestRunner::setVerbosity(aunit::Verbosity::kAll);
}

void loop() 
{
	timer1.tick();
}
