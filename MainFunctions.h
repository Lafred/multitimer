#include <Arduino.h>
// Buzzer Libraries
#include <CuteBuzzerSounds.h>
#include <Sounds.h>
// Seven Segment Display
#include <TM1637.h>
// Gyro and accelerometer
// #include <Arduino_LSM6DS3.h>
#include <SparkFunLSM6DS3.h>
#include <Wire.h>
// Timer Interupts
#include "arduino-timer.h"

//#include<bits/stdc++.h> 
  
#include "CountDownTime.h"

// Used Pins
#define DISPLAY_CLK 2
#define DISPLAY_DIO 3
#define BUZZER_PIN 7

// WiFi and Internal RTC for WiFi
//#include <WiFiUdp.h>
//#include <RTCZero.h>
//#include "arduinoSecrets.h"

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
//char ssid[] = SECRET_SSID;        // your network SSID (name)
//char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;                           // your network key Index number (needed only for WEP)
//int status = WL_IDLE_STATUS;
const int GMT = 2; //change this to adapt it to your time zone

// Create a timer that fires every "timeInt" milliseconds for Gyroscope read.
double timeInt = 250;

static float correctionConstantX = .19;
static float correctionConstantZ = 1.30;
static bool startingTimer = true;

int currentSide = 0;  

//MillisTimer secondsTimer(1000);
//static unsigned long secondsCount = 0;

// Gyroscope Variables
float x, y, z, dx, dy, dz;

// Instantiate 7 Segment display
TM1637 tm1637(DISPLAY_CLK, DISPLAY_DIO);

CountDownTime CountDownTimer;

static double position = 360/14; 

// gyro initialization 
LSM6DS3 IMU( I2C_MODE, 0x6b );


bool TIMES_UP = false;

float notesOfSong[]={ NOTE_B5, NOTE_A5, NOTE_G5, NOTE_A5, NOTE_B5, NOTE_B5, NOTE_B5, NOTE_B5,
	NOTE_A5, NOTE_A5, NOTE_A5, NOTE_A5, NOTE_B5, NOTE_D6, NOTE_D6, NOTE_D6 }; 

bool timesUp(void*) {
	static int currentNote = 0;
	int sound = 50;
	int silent =450;

	Serial.println(String("current song duration: ") + "16" );

	if(16 > currentNote && TIMES_UP == true)
	{
		Serial.println( String("current note: ") + currentNote );
		cute.playTone(notesOfSong[currentNote], sound, silent);
		currentNote++;
	}
	else if (currentNote == 16)
	{
		currentNote = 0;
		TIMES_UP = false;
	}

	return true;
}

bool getGyroDataInterrupt(void*)
{
	static float count = 0;
	//Serial.print(String("Repeat: ") + count);
	count++;
	//Serial.println(mt.getRemainingRepeats());

	//if (IMU.gyroscopeAvailable() && IMU.accelerationAvailable()) 
	//{
		z = IMU.readFloatGyroZ() + correctionConstantZ;
		//IMU.readAcceleration(dx, dy, dz);
		position = (position + (float(20) / float(1000) * (z)) );
		Serial.println(String("x= ") + z);
		Serial.println(String("Position= ") + position);
	//}
	//else {
	//	Serial.println("*****************skipped*****************");
	//}
	return true;
}

bool writeToDisplay(void*)
{
	static int remainingMinutes; 
	static int remainingSeconds;

	int current = CountDownTime::checkPosition(position);

	if (currentSide != current)
	{
		startingTimer = true;
	}

	currentSide = current;

	Serial.println(position);

	if (startingTimer == true) 
	{
		remainingMinutes = CountDownTime::getMin();
		remainingSeconds = CountDownTime::getSec();
		startingTimer = false;

	}

	if (TIMES_UP == false)
	{
		if ( remainingMinutes > 0 && remainingSeconds == 0)
		{ 
			tm1637.display(remainingMinutes*100 + remainingSeconds);
			remainingSeconds = 59; 
			remainingMinutes--;
		}
		else if (remainingMinutes > 0 || remainingSeconds > 0)
		{
			tm1637.display(remainingMinutes*100 + remainingSeconds);
			remainingSeconds--;
		}
		else
		{
			tm1637.display(remainingMinutes*100 + remainingSeconds);

			TIMES_UP = true;

			startingTimer = true;
	
		}
	}

	//repeat
	return true;
}
